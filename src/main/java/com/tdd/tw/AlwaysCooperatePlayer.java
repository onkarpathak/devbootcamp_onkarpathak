package com.tdd.tw;

public class AlwaysCooperatePlayer extends Player {

    AlwaysCooperatePlayer() {
        super.playerMove = Move.COOPERATE;
    }

    @Override
    void move() {
        super.playerMove = Move.COOPERATE;
    }
}
